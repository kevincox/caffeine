# How it works.

Right now I only use one technique as my goal was just to get my work laptop to stay awake, bypassing the org policy, when I need it to.

## Video

The website creates an autoplaying video. This keeps the screen awake at least on Firefox for MacOS.

Notes:

- The video is muted so that autoplay works.
- The video has a silent audio track, it doesn't work otherwise.
