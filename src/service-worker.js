import * as workboxGoogleAnalytics from 'workbox-google-analytics';
import * as workboxRouting from 'workbox-routing';
import * as workboxStrategies from 'workbox-strategies';

workboxGoogleAnalytics.initialize();

workboxRouting.registerRoute(
	({url}) => url.host == location.host,
	new workboxStrategies.StaleWhileRevalidate());
